+++
outputs = ["Reveal"]
+++

# RustLog

A formalization of the Polonius model checker

*by H. Haldi and M. Algelly*

---

# Monthly reports

- [Report 1](23.03.07/)
- [Report 2](23.04.04/)
- [Report 3](23.05.09/)
- [Final report](final/)
