---
theme: academic
layout: cover
coverBackgroundUrl: https://source.unsplash.com/collection/94734566/1920x1080
class: text-white text-center
highlighter: shiki
lineNumbers: true
info: |
  ## RustLog - Final Presentation
  An attempt to create a formal model of Rust's borrow checker
  using Polonius

  Learn more at [gitlab.unige.ch/aft/rustlog](https://gitlab.unige.ch/aft/rustlog)
drawings:
  persist: false
transition: slide-left
css: unocss
title: RustLog - Final Presentation
---

# RustLog - Final Presentation

An attempt to create a formal model of Rust's borrow checker
using Polonius

Learn more at [gitlab.unige.ch/aft/rustlog](https://gitlab.unige.ch/aft/rustlog)

---
layout: default
---

# Kind reminder

## The borrow checker

Rust's borrow checker is a static analysis tool that ensures memory safety

It is based on the concept of **borrowing** and **ownership** (see previous presentation)

## Polonius

While the actual borrow checker use the NLL algorithm, Polonius is a more abstract model of the borrow checker which focuses on the origin of the borrow instead of seeing the future

---
layout: default
---

# The problem

NLL is not powerful enough to express all the rules of the borrow checker

Solution: use Polonius instead

Each borrow creates a **loan** which constitutes **region** of the code. The loan is valid as long as the region is valid. A region is valid as long as it is not invalidated by a conflict.

A variable has a path and a loan is invalidated if the path is modified.

But...

---
layout: default
---

## Polonius documentation

The Polonius documentation needs to be improved:

- The documentation is not up to date
- The documentation is not clear enough
- The documentation is not complete enough
- Outdated examples

---
layout: default
---

## RustLog

...so we created RustLog, a primitive borrow checker based on a subset of Rust:

### Grammar

```
Prog ::= Stmt ';' Prog | ε
Stmt ::= 'let' Mode Var '=' Expr | '*'? Var '=' Expr | Var
Expr ::= Int | Var | '&' Mode Var
Var ::= [a-zA-Z_][a-zA-Z0-9_]*
Mode ::= 'mut' | ε
```

---

# Semantic Rules

Let there be:
- $x, y, z \in Var$
- $c, c_{1}, c_{2}, c_{3} \in Ctx$,
- $i \in Stmt$,
- $p \in Prog$,
- $j \in Int$,
- $m, m', m_1, m_2 \in Mode$
- $l, l' \in List$

---
layout: two-cols
---

## Context

![Context 1](/images/context_1.png)

::right::

![Context 2](/images/context_2.png)

---

## Conflicts

![Conflict](/images/conflict.png)

---
layout: center
---

## Program

<img src="/images/program.png" alt="Program" style="width: 80%">

---
layout: center
---

## Example

```rust
// empty
let mut x = 42;
// (x, []) :: empty
let y = &mut x;
// (y, []) :: (x, (y, mut) :: []) :: empty
let z = &x;
// (z, (y, mut) :: []) :: (y, (z, sh) :: []) :: (x, (y, mut) :: (z, sh) :: []) :: empty
*y = 13; // y is borrowed by z, however z is live => error on this line
z;
```

---
lines: true
---

# Prolog demo

(rules impemented in `prog.pl`)

## Example

```rust
let mut x = 42;
let y = mut &x;
x = 13;
y;
```

```prolog
liveUntil(y, 4).
```

```prolog
?- borrowChecker([(1, let, mut, x, 42), (2, let, immut, y, mut, x), (3, x, 13), (4, y)], [], CF).
Conflict detected at line: 3
Trying to borrow x as mut while it is borrowed as mut by y
true.
```

---

# Conclusion

## What we did

- We created a primitive borrow checker based on a subset of Rust
- We implemented the semantic rules of the borrow checker in Prolog

---

# Conclusion

## What we learned

- How the borrow checker works
- How to model the borrow checker in semantic rules
- How to implement semantic rules in Prolog

---

# Conclusion

## What to do next

- Improve the documentation of Polonius
- Make RustLog more general
- Implement more rules

---

# Questions ?
