# Grammar

```
Prog := Stmt ';' Prog | ε
Stmt := 'let' Mode Var '=' Expr | '*'? Var '=' Expr | Var
Expr := Int | Var | '&' Mode Var
Var := [a-zA-Z_][a-zA-Z0-9_]*
Mode := 'mut' | ε
```


## Example

```rust
{
    let x = 1;
    let y = x;
    let p = &x;
    *p = 2;
    
}
```

### AST

```
Prog
  Block
    Stmt
      let Var = Expr ;
        Var
          x
        Expr
          Int
            1
    Stmt
      let Var = Expr ;
        Var
          y
        Expr
          Var
            x
    Stmt
      let Var = Expr ;
        Var
          p
        Expr
          & Var
            x
    Stmt
      * Var = Expr ;
        Var
          p
        Expr
          Int
            2
```
       