mode(sh).
mode(mut).

variable(a).
variable(x).
variable(y).
variable(z).

isNotLive(X, NL):-
    liveUntil(X, NL2),
    NL2 < NL.

ctx([]).

ctx([(X,L)|T]):-
    variable(X),
    is_list(L),
    ctx(T).

init(C1, X, C2):-
	ctx(C1),
    variable(X),
    C2 = [(X,[])|C1].

add([(X,L)|T], X, Y, M, C2):-
    ctx([(X,L)|T]),
    variable(X),
    variable(Y),
    mode(M),
    C2 = [(X,[(Y,M)|L])|T].
    
add([(X1,L)|T], X2, Y, M, C2):-
    ctx([(X1,L)|T]),
    variable(X2),
    variable(Y),
    mode(M),
    X1 \= X2,
    add(T, X2, Y, M, C3),
    C2 = [(X1,L)|C3].

addInv([], C, _, _, _, C).

addInv([(X,[])|_], C, X, _, _, C).

addInv([(X,[(Y,M)|L])|T], C, X, Y, M, C2):-
    addInv([(X,L)|T], C, X, Y, M, C2).

addInv([(X,[(Y2,M1)|L])|T], C, X, Y, M2, C4):-
    Y \= Y2,
    add(C, Y, Y2, M1, C2),
    add(C2, Y2, Y, M2, C3),
    addInv([(X,L)|T], C3, X, Y, M2, C4).

addInv([(X1,_)|T], C, X2, Y, M, C2):-
    X1 \= X2,
    addInv(T, C, X2, Y, M, C2).

hasNoConflict([], X, _, M):-
    variable(X),
    mode(M).

hasNoConflict([(X,[])|T], X, _, M):-
    ctx([(X,[])|T]),
    variable(X),
    mode(M).

hasNoConflict([(X,[(Y,M1)|L])|T], X, NL, M2):-
    ctx([(X,[(Y,M1)|L])|T]),
    variable(X),
    mode(M2),
    (   
        M1 == mut ; 
        M2 == mut
    ), 
    \+ isNotLive(Y, NL),
    throw(error(conflict_detected, NL, X, Y, M1, M2)).

hasNoConflict([(X,[(Y,M1)|L])|T], X, NL, M2):-
    ctx([(X,[(Y,M1)|L])|T]),
    variable(X),
    mode(M2),
    (
        (M1 \= mut, M2 \= mut) ; 
        isNotLive(Y, NL)
    ),
    hasNoConflict([(X,L)|T], X, NL, M2).

hasNoConflict([(X1,L)|T], X2, NL, M2):-
    ctx([(X1,L)|T]),
    variable(X2),
    mode(M2),
    X1 \= X2,
    hasNoConflict(T, X2, NL, M2).

borrowCheck([], CF, CF).

borrowCheck([(_, let, _, Y, M2, X)|T], C, CF):-
    init(C, Y, C2),
    add(C2, X, Y, M2, C3),
    addInv(C3, C3, X, Y, M2, C4),
    borrowCheck(T, C4, CF).

borrowCheck([(_, let, _, X, _)|T], C, CF):-
    init(C, X, C2),
    borrowCheck(T, C2, CF).

borrowCheck([(NL, X, _)|T], C, CF):-
    hasNoConflict(C, X, NL, mut),
    borrowCheck(T, C, CF).

borrowCheck([(NL, X)|T], C, CF):-
    hasNoConflict(C, X, NL, sh),
    borrowCheck(T, C, CF).

throw_error(NL, X, Y, M1, M2) :-
    throw(error(conflict_detected, NL, X, Y, M1, M2)).

borrowChecker(P, C, CF):-
    catch(
        borrowCheck(P, C, CF), 
        error(conflict_detected, NL, X, Y, M1, M2),
        (write('Conflict detected at line: '), 
        print(NL), 
        write('\nTrying to borrow '),
        print(X),
        write(' as '),
        print(M2),
        write(' while it is borrowed as '),
        print(M1),
        write(' by '),
        print(Y),
        nl)
    ).

% liveUntil(y, 4).
% borrowChecker([(1, let, mut, x, 42), (2, let, immut, y, mut, x), (3, x, 13), (4, y)], [], CF).

% liveUntil(y, 2).
% liveUntil(z, 6).
% borrowChecker([(1, let, mut, x, 42), (2, let, immut, y, mut, x), (3, x, 13), (4, let, immut, z, sh, x), (5, x, 12), (6, z)], [], CF).

liveUntil(y, 5).
liveUntil(z, 6).
% borrowChecker([(1, let, mut, x, 42), (2, let, immut, y, mut, x), (3, let, immut, z, sh, x), (4, y, 24), (5, z)], [], CF).

% liveUntil(y, 4).
% liveUntil(z, 3).
% borrowChecker([(1, let, mut, x, 42), (2, let, immut, y, mut, x), (3, let, immut, z, sh, x), (4, y, 24)], [], CF).

% liveUntil(y, 4).
% liveUntil(z, 3).
% liveUntil(a, 6).
% borrowChecker([(1, let, mut, x, 42), (2, let, immut, y, mut, x), (3, let, immut, z, sh, x), (4, y, 24), (5, let, immut, a, mut, y), (6, a)], [], CF).

