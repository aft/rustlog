// struct Node<T> {
//     elem: T,
//     next: Option<Box<Node<T>>>
// }

// struct LinkedList<T> {
//     start: Option<Node<T>>,
//     end: Option<Node<T>>
// }

// impl<T> LinkedList<T> {
//     fn push_front(&mut self, elem: T) {
//         let node = Node{
//             elem: elem,
//             next: None
//         };
//         if self.start.is_none() {
//             self.start = Some(node)
//         }
//     }
// }

// fn copy_int() {
//     let x = 5;
//     let mut y = x;

//     y = 6;
//     // print x and y
//     println!("x: {}, y: {}", x, y);
// }

// fn move_string() {
//     let s = String::from("hello");
//     let t = s;

//     println!("s: {}, t: {}", s, t);
// }

// fn borrow_string() {
//     let s = String::from("hello");
//     let t = &s;

//     // print s and t memory address
//     println!("s: {:p}, t: {:p}", &s, t);
// }

// use std::collections::HashMap;

// fn get_or_insert(map: &mut HashMap<u32, String>) -> &String {
//     match map.get(&22) {
//         Some(v) => v,
//         None => {
//             map.insert(22, String::from("hi"));
//             &map[&22]
//         }
//     }
// }

struct S<'a> {
    x: &'a i32,
}

fn main() {
    // let mut v = vec![&1, &2, &3];
    // let mut x = 42;
    // Vec::push(&mut v, &x);
    // x = 13;
    // v;
    let mut x = 42;
    let p = &mut x;
    x;
    p;
}