\documentclass{article}

% Language setting
% Replace `english' with e.g. `spanish' to change the document language
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
% Set page size and margins
% Replace `letterpaper' with `a4paper' for UK/EU standard size
\usepackage[letterpaper,top=2cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

% Useful packages
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{ {./images/} }
\usepackage{svg}
\usepackage{wrapfig}
\usepackage{inputenc}
\usepackage{minted}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{subcaption}
\usepackage{bussproofs}
\usepackage{plantuml}
%\usepackage[skip=10pt plus1pt, indent=40pt]{parskip}

\usepackage{parskip}
\usepackage{pdflscape}
\usepackage{color}


\begin{document}
\begin{titlepage}
    \begin{center}
        \vspace*{4cm}

        \Huge
        Advanced Formal Tools

        \Huge
        \textbf{RustLog}

        \vspace{0.5cm}
        \Large
        \emph{ A formal modelisation of the \\
        new Polonius Rust borrow checker }

        \vspace{0.5cm}
        \url{https://gitlab.unige.ch/aft/rustlog}

        \vspace{1.5cm}

        \vfill
        \vspace{0.2cm}

        \includegraphics[width=0.6\textwidth]{logo.png}

        \Large
        H. Haldi \& M. Algelly \\
        \vspace{0.5cm}
        Computer Science Department  \\
        University of Geneva \\
        Geneva, Switzerland \\
        Spring 2023
    \end{center}
\end{titlepage}
\newpage

\tableofcontents

\newpage

\section{Introduction}

The Rust programming language is renowned for its systems programming capabilities, enabling developers to write robust, efficient, and safe code. One of the key features of Rust is its innovative system for handling memory management and data race conditions, known as the borrow checker. This is where Polonius \cite{polonius:blogpost} comes into play, as the latest development in the evolution of Rust's borrow checker.

Our original plan was to formalize Polonius, the new borrow checker of Rust \cite{polonius:book}, focusing on the intricate rules that ensure safe memory accesses while avoiding data races and null pointer dereferencing \cite{polonius:talk}. However, Polonius' intricate nature and the complexity of Rust as a whole presented an arduous task beyond the scope of our current capabilities.

With the complexity in mind, we decided to focus our efforts on formalizing a more simplified model. This model represents a small subset of Rust, a language that we will refer to as RustLog. Although this language does not include the entirety of Rust's complexities and features, it incorporates the fundamental aspects of Rust's borrow checker, creating a reasonable stepping stone towards understanding and eventually formalizing Polonius.

The rest of this document introduces the syntax and semantics of RustLog, along with examples. 

\section{Syntax}

\begin{verbatim}
    Prog := Stmt ';' Prog | e
    Stmt := 'let' Var '=' Expr | '*'? Var '=' Expr | Var
    Expr := Int | Var | '&' Mode Var
    Var := [a-zA-Z_][a-zA-Z0-9_]*
    Mode := 'mut' | e
\end{verbatim}

\newpage

\section{Semantics}

Let there be: \\ 
$x, y, z \in Var$, \\
$c, c_{1}, c_{2}, c_{3} \in Ctx$,\\ 
$i \in Stmt$, \\
$p \in Prog$, \\
$j \in Int$, \\
$m, m', m_1, m_2 \in Mode$\\
$l, l' \in List$

\subsection{Context}

\begin{prooftree}
    \AxiomC{}
    \LeftLabel{{\scriptsize(1)}}
    \UnaryInfC{empty $\implies$ empty}
\end{prooftree}

\begin{prooftree}
    \AxiomC{}
    \LeftLabel{{\scriptsize(2)}}
    \UnaryInfC{init(c, x) $\implies$ (x, []) :: c}
\end{prooftree}

\begin{prooftree}
    \AxiomC{}
    \LeftLabel{{\scriptsize(3)}}
    \UnaryInfC{add((x, l) :: c, x, y, m) $\implies$ (x, (y, m) :: l) :: c}
\end{prooftree}

\begin{prooftree}
    \AxiomC{$x_{1}$ $\neq$ $x_{2}$}
    \AxiomC{add(c, $x_{2}$, y, m) $\implies$ $c_{1}$} 
    \LeftLabel{{\scriptsize(4)}}
    \BinaryInfC{add(($x_{1}$, l) :: c, $x_{2}$, y, m) $\implies$ ($x_{1}$, l) :: $c_{1}$}
\end{prooftree}

\begin{prooftree}
    \AxiomC{$x_{1} \neq x_{2}$}
    \AxiomC{addInv(c, $c_{1}$, $x_{2}$, y, m) $\implies$ $c_{2}$}
    \LeftLabel{{\scriptsize(5)}}
    \BinaryInfC{addInv(($x_{1}$, l) :: c, $c_{1}$ , $x_{2}$, y, m) $\implies$ $c_{2}$}
\end{prooftree}

\begin{prooftree}
    \AxiomC{addAll($c_{1}$, y, l, m) $\implies$ $c_{2}$}
    \LeftLabel{{\scriptsize(6)}}
    \UnaryInfC{addInv((x, l) :: c, $c_{1}$, x, y, m) $\implies$ $c_{2}$}
\end{prooftree}

\begin{prooftree}
    \AxiomC{}
    \LeftLabel{{\scriptsize(7)}}
    \UnaryInfC{addAll(c, y, [], m) $\implies$ c}
\end{prooftree}
    
\begin{prooftree}
    \AxiomC{addAll(c, y, l, m) $\implies$ $c_{1}$}
    \AxiomC{add($c_{1}$, x, y, m) $\implies$ $c_{2}$}
    \AxiomC{add($c_{2}$, y, x, m') $\implies$ $c_{3}$}
    \LeftLabel{{\scriptsize(8)}}
    \TrinaryInfC{addAll(c, y, (x, m')::l, m) $\implies$ $c_{3}$}
\end{prooftree}

\begin{prooftree}
    \AxiomC{addAll(c, x, l, m) $\implies$ $c_{1}$}
    \LeftLabel{{\scriptsize(9)}}
    \UnaryInfC{addAll(c, x, (x, m')::l, m) $\implies$ $c_{1}$}
\end{prooftree}

\subsection{Conflict}

\begin{prooftree}
    \AxiomC{}
    \LeftLabel{{\scriptsize(1)}}
    \UnaryInfC{hasConflict(empty, x, m) $\implies$ false}
\end{prooftree}

\begin{prooftree}
    \AxiomC{}
    \LeftLabel{{\scriptsize(2)}}
    \UnaryInfC{hasConflict((x, []) :: c, x, m) $\implies$ false}
\end{prooftree}

\begin{prooftree}
    \AxiomC{$m_{1} = \mathrm{mut} \lor m_{2} = \mathrm{mut}$}
    \AxiomC{isLive(y) $\implies$ true}
    \LeftLabel{{\scriptsize(3)}}
    \BinaryInfC{hasConflict((x, (y, $m_{1}$) :: l) :: c, x, $m_{2}$) $\implies$ true}
\end{prooftree}

\begin{prooftree}
    \AxiomC{$m_{1} = \mathrm{mut} \lor m_{2} = \mathrm{mut}$}
    \AxiomC{isLive(y) $\implies$ false}
    \AxiomC{hasConflict((x, l) :: c, x, $m_{2}$) $\implies$ b}
    \LeftLabel{{\scriptsize(4)}}
    \TrinaryInfC{hasConflict((x, (y, $m_{1}$) :: l) :: c, x, $m_{2}$) $\implies$ b}
\end{prooftree}

\begin{prooftree}
    \AxiomC{$m_{1} \neq \mathrm{mut} \land m_{2} \neq \mathrm{mut}$}
    \AxiomC{hasConflict((x, l) :: c, x, $m_{2}$) $\implies$ b}
    \LeftLabel{{\scriptsize(5)}}
    \BinaryInfC{hasConflict((x, (y, $m_{1}$) :: l) :: c, x, $m_{2}$) $\implies$ b}
\end{prooftree}

\begin{prooftree}
    \AxiomC{$x_{1} \neq x_{2}$}
    \AxiomC{hasConflict(c, $x_{2}$, m) $\implies$ b}
    \LeftLabel{{\scriptsize(6)}}
    \BinaryInfC{hasConflict(($x_{1}$, l) :: c, $x_{2}$, m) $\implies$ b}
\end{prooftree}

\subsection{Program}

\begin{prooftree}
    \AxiomC{}
    \LeftLabel{{\scriptsize(1)}}
    \UnaryInfC{c $\vdash \epsilon \implies$ c}
\end{prooftree}

\begin{prooftree}
    \AxiomC{init(c, x) $\implies$ $c_{1}$}
    \LeftLabel{{\scriptsize(2)}}
    \UnaryInfC{c $\vdash$ let m x = j $\implies$ $c_{1}$}
\end{prooftree}

\begin{prooftree}
    \AxiomC{init(c, y) $\implies$ $c_{1}$}
    \AxiomC{add($c_{1}$, x, y, m) $\implies$ $c_{2}$}
    \AxiomC{addInv($c_{2}$, $c_{2}$, x, y, m) $\implies$ $c_{3}$}
    \LeftLabel{{\scriptsize(3)}}
    \TrinaryInfC{c $\vdash$ let y = \&m x $\implies$ $c_{3}$}
\end{prooftree}

\begin{prooftree}
    \AxiomC{hasConflict(c, x, mut) $\implies$ false}
    \LeftLabel{{\scriptsize(4)}}
    \UnaryInfC{c $\vdash$ x = j $\implies$ c}
\end{prooftree}

\begin{prooftree}
    \AxiomC{hasConflict(c, x, mut) $\implies$ true}
    \LeftLabel{{\scriptsize(5)}}
    \UnaryInfC{c $\vdash$ x = j $\implies$ fail}
\end{prooftree}

\begin{prooftree}
    \AxiomC{hasConflict(c, x, sh) $\implies$ false}
    \LeftLabel{{\scriptsize(6)}}
    \UnaryInfC{c $\vdash$ x $\implies$ c}
\end{prooftree}

\begin{prooftree}
    \AxiomC{hasConflict(c, x, sh) $\implies$ true}
    \LeftLabel{{\scriptsize(7)}}
    \UnaryInfC{c $\vdash$ x $\implies$ fail}
\end{prooftree}

\begin{prooftree}
    \AxiomC{c $\vdash$ i; $\implies$ $c_{1}$}
    \AxiomC{$c_{1}$ $\vdash$ p $\implies$ $c_{2}$}
    \LeftLabel{{\scriptsize(8)}}
    \BinaryInfC{c $\vdash$ i; p $\implies$ $c_{2}$}
\end{prooftree}

\bibliographystyle{plain} % We choose the "plain" reference style
\bibliography{refs} % Entries are in the refs.bib file

\end{document}